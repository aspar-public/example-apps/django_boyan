from django.contrib.auth import get_user_model
from rest_framework import serializers
from backend.products.models import Product

UserModel = get_user_model()


class OwnerSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    login = serializers.CharField(required=True)

    def to_internal_value(self, data):
        try:
            user_id = int(data.get('id'))
            return UserModel.objects.get(pk=user_id)
        except (ValueError, UserModel.DoesNotExist):
            raise serializers.ValidationError('Invalid user id')

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'login': instance.login
        }


class ProductAddSerializer(serializers.ModelSerializer):
    owner = OwnerSerializer()

    class Meta:
        model = Product
        fields = [
            'id',
            'code',
            'name',
            'description',
            'price',
            'image01',
            'image01ContentType',
            'owner'
        ]

    image01 = serializers.CharField(source='image_01', required=False)
    image01ContentType = serializers.CharField(source='image_01_content_type', required=False)

    def create(self, validated_data):
        owner_data = validated_data.pop('owner', None)
        instance = super().create(validated_data)

        if owner_data:
            instance.owner = owner_data

        return instance
