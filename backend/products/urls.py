from django.urls import path

from backend.products.views import CreateProductAPIView

urlpatterns = [
    path('', CreateProductAPIView.as_view(), name='product-create'),

]