from django.contrib import admin

from backend.products.models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass
