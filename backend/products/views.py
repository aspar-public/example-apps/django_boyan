from rest_framework import generics, status
from rest_framework.response import Response

from backend.products.models import Product
from backend.products.serializers import ProductAddSerializer


class CreateProductAPIView(generics.CreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductAddSerializer


