from django.contrib import admin

from backend.user.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass
