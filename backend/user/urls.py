from django.urls import path

from backend.user.views import UserRegistration, ActivateAccount, AdminSearchUsersView, CustomTokenObtainPairView


urlpatterns = [
    path('register/', UserRegistration.as_view(), name='user-registration'),
    path('activate?key=<str:key>', ActivateAccount.as_view(), name='activate-account'),
    path('authenticate/', CustomTokenObtainPairView.as_view(), name='login-account-token-obtain'),
    path('admin/users', AdminSearchUsersView.as_view(), name='search-users')
]