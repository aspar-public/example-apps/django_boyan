from django.contrib.auth import get_user_model
from backend import settings
from django.urls import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags


UserModel = get_user_model()


@receiver(post_save, sender=UserModel)
def send_welcome_email(sender, instance, created, **kwargs):
    if created:
        subject = 'Django Application account activation'

        context = {
            'user': instance,
            'HOST': settings.HOST,
        }
        html_message = render_to_string('email/registration-email.html', context)
        plain_message = strip_tags(html_message)

        from_email = settings.EMAIL_HOST_USER
        recipient_list = [instance.email]
        # print('Subject', subject, '\nfrom_email', from_email, '\nrecipient_list', recipient_list, '\nplain_message', plain_message, '\nhtml_message', html_message)
        send_mail(subject, plain_message, from_email, recipient_list, html_message=html_message)