import uuid
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from backend.user.managers import CustomUserManager


class User(AbstractBaseUser, PermissionsMixin):
    LANGUAGES = [
        ('en', 'en'),
        ('bg', 'bg'),
        ('fr', 'fr'),
        ('de', 'de'),
    ]
    id = models.BigAutoField(
        primary_key=True,
        null=False,
    )
    login = models.CharField(
        max_length=50,
        unique=True,
    )
    password = models.CharField(
        max_length=67,
    )
    first_name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
    )
    last_name = models.CharField(
        max_length=50,
        null=True,
        blank=True,
    )
    email = models.CharField(
        max_length=191,
        unique=True,
    )
    image_url = models.CharField(
        max_length=256,
        null=True,
        blank=True,
    )
    activated = models.BooleanField(
        default=False,
        null=False,
    )
    lang_key = models.CharField(
        max_length=10,
        choices=LANGUAGES,
        null=True,
        blank=True,
    )
    activation_key = models.CharField(
        max_length=20,
        null=True,
        blank=True,
    )
    reset_key = models.CharField(
        max_length=20,
        null=True,
        blank=True,
    )
    reset_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    created_by = models.CharField(
        max_length=50,
        default='anonymousUser',
        null=False,
    )
    created_date = models.DateTimeField(
        auto_now_add=True,
    )
    last_modified_by = models.CharField(
        max_length=50,
        default='anonymousUser',
        null=True,
        blank=True,
    )
    last_modified_date = models.DateTimeField(
        auto_now=True,
    )
    is_staff = models.BooleanField(
        default=False,
    )

    objects = CustomUserManager()

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['email']

    def save(self, *args, **kwargs):
        if not self.id:
            while True:
                activation_key = str(uuid.uuid4().hex)[:20]
                if not User.objects.filter(activation_key=activation_key).exists():
                    self.activation_key = activation_key
                    break

            while True:
                reset_key = str(uuid.uuid4().hex)[:20]
                if not User.objects.filter(reset_key=reset_key).exists():
                    self.reset_key = reset_key
                    break
        super(User, self).save(*args, **kwargs)