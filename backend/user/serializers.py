from rest_framework import serializers
from backend.user.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'login',
            'firstName',
            'lastName',
            'email',
            'imageUrl',
            'langKey',
            'password'
        ]

    firstName = serializers.CharField(source='first_name')
    lastName = serializers.CharField(source='last_name')
    imageUrl = serializers.CharField(source='image_url')
    langKey = serializers.CharField(source='lang_key')

    def create(self, validated_data):
        new_user = User.objects.create_user(**validated_data)
        return new_user


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'login',
            'firstName',
            'lastName',
            'email',
            'imageUrl',
            'activated',
            'langKey',
            'createdBy',
            'createdDate',
            'lastModifiedBy',
            'lastModifiedDate',
            'authorities'
        ]

    firstName = serializers.CharField(source='first_name')
    lastName = serializers.CharField(source='last_name')
    imageUrl = serializers.CharField(source='image_url')
    langKey = serializers.CharField(source='lang_key')
    createdBy = serializers.CharField(source='created_by')
    createdDate = serializers.CharField(source='created_date')
    lastModifiedBy = serializers.CharField(source='last_modified_by')
    lastModifiedDate = serializers.CharField(source='last_modified_date')
    authorities = serializers.SerializerMethodField()

    @staticmethod
    def get_authorities(obj):
        return list(obj.groups.all())


class YourTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        return token
