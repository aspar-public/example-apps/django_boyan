from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http import HttpResponseBadRequest
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from backend.user.serializers import UserSerializer, YourTokenObtainPairSerializer, UserListSerializer
from rest_framework_simplejwt.views import TokenObtainPairView


User = get_user_model()


class UserRegistration(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            response_data = {
                "id": user.id,
                "login": user.login,
                "firstName": user.first_name,
                "lastName": user.last_name,
                "email": user.email,
                "imageUrl": user.image_url,
                "langKey": user.lang_key,
                'activated': user.activated,
                'createdBy': user.created_by,
                'createdDate': user.created_date,
                'lastModifiedBy': user.last_modified_by,
                'lastModifiedDate': user.last_modified_date,
                "authorities": list(user.groups.all())
            }
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ActivateAccount(APIView):
    def get(self, request, *args, **kwargs):
        activation_key = kwargs['key']

        if not activation_key:
            return HttpResponseBadRequest('Activation key is missing')

        try:
            user = User.objects.get(activation_key=activation_key, activated=False)
        except User.DoesNotExist:
            return HttpResponseBadRequest('Invalid activation key or user already activated')

        user.activated = True
        user.last_modified_by = user.login
        user.save()
        response_data = {
                "id": user.id,
                "login": user.login,
                "firstName": user.first_name,
                "lastName": user.last_name,
                "email": user.email,
                "imageUrl": user.image_url,
                "langKey": user.lang_key,
                'createdBy': user.created_by,
                'createdDate': user.created_date,
                'lastModifiedBy': user.last_modified_by,
                'lastModifiedDate': user.last_modified_date,
                "authorities": list(user.groups.all())
            }
        return Response(response_data, status=status.HTTP_200_OK)


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = YourTokenObtainPairSerializer


class AdminSearchUsersView(APIView):
    permission_classes = [IsAuthenticated, IsAdminUser]
    FIELDS_MAP = {
        'login': 'login',
        'firstName': 'first_name',
        'lastName': 'last_name'
    }

    OPERATORS_MAP = {
        'contains': 'contains',
        'doesNotContain': 'contains',
        'equals': 'exact',
        'notEquals': 'exact',
        'specified': 'isnull',
        'in': 'in',
        'notIn': 'in'
    }

    def get(self, request):
        query = Q()
        for param, value in request.query_params.items():
            field_operator = param.split('.')
            if len(field_operator) == 2:
                field, operator = field_operator
                if operator in ['in', 'notIn']:
                    value = value.split(',')
                lookup_expr = f'{self.FIELDS_MAP[field]}__{self.OPERATORS_MAP[operator]}'

                if operator in ['doesNotContain', 'notEquals', 'notIn']:
                    exclude_query = Q(**{lookup_expr: value})
                    query &= ~exclude_query
                else:
                    if operator == 'specified':
                        if value.lower() == 'true':
                            value = True
                        elif value.lower() == 'false':
                            value = False

                    query &= Q(**{lookup_expr: value})
            else:
                raise Exception('Wrong parameters of the search request')
        queryset = User.objects.filter(query)
        response_data = UserListSerializer(queryset, many=True)
        return Response(response_data.data, status=status.HTTP_200_OK)

